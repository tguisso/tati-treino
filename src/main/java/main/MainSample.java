package main;

import br.com.tatiguisso.tatitreino.sample.Calculadora;

public class MainSample {

	
	public static void main(String[] args) {
		Calculadora calc = new Calculadora();
		
		int varA = 10;
		int varB = 15;
		
		int result = calc.soma(varA, varB);
		
		System.out.println(result);
	}
	
	
}
