package br.com.tatiguisso.tatitreino.exercicio1007;

public class Orquestrador {

	private Soma somador;
	private Subtracao diminuir;

	public String executar(int num1, int num2, int num3, int num4) {

		if (somador.somar(num1, num2, num3, num4)> 10) {
			return "sucesso";

		}

		int subtracao = diminuir.subtrair(num1, num2, num3, num4);

		if (subtracao < 1) {
			return "erro";
		}


		return "invalido";

	}
}
