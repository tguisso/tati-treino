package br.com.tatiguisso.tatitreino.exercicio1007;

import java.util.Scanner;

public class Aplicacao {

	public static void main(String[] args) {
		Diferenca diferenca = new Diferenca();
		Scanner sc = new Scanner(System.in);
		
		int param1;
		int param2;
		int param3;
		int param4;
		
		param1 = sc.nextInt();
		param2 = sc.nextInt();
		param3 = sc.nextInt();
		param4 = sc.nextInt();

		
		int resultado = diferenca.execute(param1, param2, param3, param4);
		System.out.println(resultado);
		
		sc.close();		

	}

}
