package br.com.tatiguisso.tatitreino.renan.exercicio04.usecase;

import br.com.tatiguisso.tatitreino.renan.exercicio04.domains.Client;
import br.com.tatiguisso.tatitreino.renan.exercicio04.domains.Product;

public class NewCart {

	public boolean createCart (Client client, Product product, int quantity) {

		VerifyAge verifyAge = new VerifyAge();

		boolean isOver18 = verifyAge.checkAge(client.idade);

		if (isOver18 == true) {
			
			ExistingClient existingClient = new ExistingClient();
			boolean cpfCadastrado = existingClient.verifyByCpf(client.cpf);
			
			if (cpfCadastrado == false) {
				return false;
			}
			
			return true;
		} 
		else {
			return false;
		}

	}

}
