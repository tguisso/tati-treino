package br.com.tatiguisso.tatitreino.renan.exercicio04.domains;

import java.time.LocalDate;

public class Cart {
	
	public long id;
	public LocalDate date;
	public Client client;
	//products [products];
	public double freight;
	public double price;

}
