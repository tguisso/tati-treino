package br.com.tatiguisso.tatitreino.renan.exercicio04.usecase;

import br.com.tatiguisso.tatitreino.renan.exercicio04.domains.Client;
import br.com.tatiguisso.tatitreino.renan.exercicio04.domains.Product;

public class Main {

	public static void main(String[] args) {
		
		NewCart newCart = new NewCart();
		
		Client client = new Client();
		client.idade = 15; 
		client.idade = 45;
		client.idade = 4;
		
		int a = 10;
		a = 13;
		a = 9;		
		Product product = new Product();
		int quantity = 0;
		
		boolean isCartCreatedSuccessfully = newCart.createCart(client, product, quantity);
		
		if (isCartCreatedSuccessfully == true) {
			System.out.println("Carrinho criado com sucesso!");
		}
		else {
			System.out.println("Compra negada.");
		}

	}

}
