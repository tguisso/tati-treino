package br.com.tatiguisso.tatitreino.renan.exercicio02;

public class MainMetodosEncadeados {

	public static void main(String[] args) {
		
		ClasseB classeB = new ClasseB();
		ClasseD classeD = new ClasseD();
		
		classeD.metodo1();
		classeB.metodo2();
		

	}

}
