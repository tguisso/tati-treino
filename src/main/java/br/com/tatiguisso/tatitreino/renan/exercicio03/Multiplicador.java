package br.com.tatiguisso.tatitreino.renan.exercicio03;

public class Multiplicador {
	
	public int multiplica(int x, int y, int somaResult) {
		
		int multiplicaResult = x * y * somaResult; 
		
		logarMultiplicacao(multiplicaResult);
		
		return multiplicaResult;
		
	}
	
	public void logarMultiplicacao(int multiplicaResult) {
		System.out.println("Resultado Multiplicação: "+ multiplicaResult);
	}

}
