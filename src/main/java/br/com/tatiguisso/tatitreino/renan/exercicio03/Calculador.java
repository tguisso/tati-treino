package br.com.tatiguisso.tatitreino.renan.exercicio03;

public class Calculador {
	
	public int calcular(int x, int y) {

		Somador somador = new Somador();
		Multiplicador multiplicador = new Multiplicador();
		
		logar(x,y);
				
		int somaResult = somador.soma(x, y);
		
		int multiplicaResult = multiplicador.multiplica(x, y, somaResult);
		
		return multiplicaResult;
		
	}
	
	public void logar(int x, int y) {
		
		System.out.println("x: "+ x);
		System.out.println("y: "+ y);
		
	}
	
}