package br.com.tatiguisso.tatitreino.renan.exercicio02;

public class ClasseA {
	
	public void metodo1 () {
		System.out.println("Classe A, metodo 1");
		
		ClasseC classeC = new ClasseC();
		
		classeC.metodo1();
	}

}
