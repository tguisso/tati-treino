package br.com.tatiguisso.tatitreino.renan.exercicio01;

public class MainChamadaMetodos {

	public static void main(String[] args) {
		
		ClasseA classeA = new ClasseA();
		ClasseB classeB = new ClasseB();
		ClasseC classeC = new ClasseC();
		ClasseD classeD = new ClasseD();

		classeA.metodo1();
		classeC.metodo1();
		classeC.metodo2();
		classeB.metodo1();
		classeD.metodo1();		
		
	}

}
