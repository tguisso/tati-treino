package br.com.tatiguisso.tatitreino.renan.exercicio02;

public class ClasseB {

	public void metodo1 () {
		System.out.println("Classe B, metodo 1");
	}

	public void metodo2() {
		System.out.println("Classe B, metodo 2");

		ClasseA classeA = new ClasseA();

		metodo1();
		classeA.metodo1();
	}

}
