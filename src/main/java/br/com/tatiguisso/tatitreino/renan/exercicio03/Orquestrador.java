package br.com.tatiguisso.tatitreino.renan.exercicio03;

public class Orquestrador {
	
	public int executar(int x, int y) {
		
		Calculador calculador = new Calculador();
		
		int resultado = calculador.calcular(x, y);
		return resultado;
		
	}

}
