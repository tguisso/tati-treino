package br.com.tatiguisso.tatitreino.renan.exercicio02;

public class ClasseD {
	
	public void metodo1() {
		System.out.println("Classe D, metodo 1");
		
		ClasseC classeC = new ClasseC();
		
		classeC.metodo1();		
		
	}
	
	public void metodo2() {
		System.out.println("Classe D, metodo 2");
	}

}
