package br.com.tatiguisso.tatitreino.renan.exercicio03;

public class Somador {
	
	public int soma (int x, int y) {
		int result = x + y;

		logarSoma(result);
		
		return result;
		
	}
	
	public void logarSoma(int resultadoSoma) {
		System.out.println("Resultado Soma: "+ resultadoSoma);
		
	}

}
