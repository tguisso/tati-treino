package br.com.tatiguisso.tatitreino.exercicio1021;

import java.util.Locale;
import java.util.Scanner;

public class NotasMoedas {


	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		double N;
		double nota;
		double moeda;
		int qtdNota;
		int qtdMoeda;
		double resto;

		N = sc.nextDouble();

		nota = 100.0;
		qtdNota = (int)(N / nota);
		System.out.println(qtdNota + " nota(s) de R$ 100.00");
		resto = N % 100.0;

		nota = 50.0;
		qtdNota = (int)(resto / nota);
		System.out.println(qtdNota + " nota(s) de R$ 50.00");
		resto = resto % 50.0;

		nota = 20.0;
		qtdNota = (int)(resto / nota);
		System.out.println(qtdNota + " nota(s) de R$ 20.00");
		resto = resto % 20.0;
		
		nota = 10.0;
		qtdNota = (int)(resto / nota);
		System.out.println(qtdNota + " nota(s) de R$ 10.00");
		resto = resto % 10.0;
		
		nota = 5.0;
		qtdNota = (int)(resto / nota);
		System.out.println(qtdNota + " nota(s) de R$ 5.00");
		resto = resto % 5.0;

		nota = 2.0;
		qtdNota = (int)(resto / nota);
		System.out.println(qtdNota + " nota(s) de R$ 2.00");
		resto = resto % 2.0;
		
		moeda = 1.0;
		qtdMoeda = (int) (resto / moeda);
		System.out.println(qtdMoeda + " moeda(s) de R$ 1.00");
		resto = resto % 1.0;
		
		moeda = 0.5;
		qtdMoeda = (int) (resto / moeda);
		System.out.println(qtdMoeda + " moeda(s) de R$ 0.50");
		resto = resto % 0.5;
		
		moeda = 0.25;
		qtdMoeda = (int) (resto / moeda);
		System.out.println(qtdMoeda + " moeda(s) de R$ 0.25");
		resto = resto % 0.25;
		
		moeda = 0.10;
		qtdMoeda = (int) (resto / moeda);
		System.out.println(qtdMoeda + " moeda(s) de R$ 0.10");
		resto = resto % 0.10;
		
		moeda = 0.05;
		qtdMoeda = (int) (resto / moeda);
		System.out.println(qtdMoeda + " moeda(s) de R$ 0.05");
		resto = resto % 0.05;
		
		moeda = 0.01;
		qtdMoeda = (int) (resto / moeda);
		System.out.println(qtdMoeda + " moeda(s) de R$ 0.01");
		resto = resto % 0.01;
		

		sc.close();
	}

}
