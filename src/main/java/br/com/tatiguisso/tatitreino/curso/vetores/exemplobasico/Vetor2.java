package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Scanner;

public class Vetor2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[] vet = new int[n];

		for(int i=0; i<n; i++) {
			vet[i] = sc.nextInt();
		}

		for(int i=0; i<n; i++) {
			if(vet[i] % 2 == 0) {
				System.out.print(vet[i]+ " ");
			}
		}
		System.out.println();

		int count = 0;
		for(int i=0; i<n; i++) {
			if(vet[i] % 2 == 0){
				count++;
			}
		}
		System.out.print(count + " ");


		sc.close();
	}

}
