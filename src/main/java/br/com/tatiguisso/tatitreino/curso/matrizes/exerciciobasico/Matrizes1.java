package br.com.tatiguisso.tatitreino.curso.matrizes.exerciciobasico;

import java.util.Scanner;

public class Matrizes1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int m = sc.nextInt();
		int n = sc.nextInt();
		
		int[][] mat = new int[m][n];
		int numNegativo = 0;
		
		for(int i=0; i<m; i++) {
			for(int j=0; j<n; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		
		System.out.println("VAlores negativos: ");
		
		for(int i=0; i<m; i++) {
			for(int j=0; j<n; j++) {
				if(mat[i][j] < 0) {
					System.out.println(numNegativo = mat[i][j]);
				}
			}
		}
		
						
		sc.close();

	}

}
