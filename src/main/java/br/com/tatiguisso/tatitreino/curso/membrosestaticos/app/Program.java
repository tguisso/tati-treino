package br.com.tatiguisso.tatitreino.curso.membrosestaticos.app;

import java.util.Locale;
import java.util.Scanner;

import br.com.tatiguisso.tatitreino.curso.membrosestaticos.util.CurrencyConverter;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("What is the dollar price? ");
		double price = sc.nextDouble();
		System.out.print("How many dollars will be bought? ");
		double bought = sc.nextDouble();
		
		//double dollars = CurrencyConverter.amountToBePaidInReais(price, bought);
		
		System.out.println("Amount to be paid in reais = " + CurrencyConverter.amountToBePaidInReais(price, bought));
		
		
		sc.close();
		
	}

}
