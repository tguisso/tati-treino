package br.com.tatiguisso.tatitreino.curso.vetores.exemplo1;

import java.util.Locale;
import java.util.Scanner;

public class AppExemplo1 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		double [] vect = new double[n];
		double soma = 0.0;
		double media = 0.0;
		
		for(int i=0; i<n; i++) {
			vect[i]= sc.nextDouble();
			soma += vect[i];
		}
		
		media = soma/n;
		System.out.printf("AVERAGE HEIGHT: %.2f%n", media);		
		
		sc.close();
	}

}
