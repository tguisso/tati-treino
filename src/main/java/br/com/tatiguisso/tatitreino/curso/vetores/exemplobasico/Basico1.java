package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Locale;
import java.util.Scanner;

public class Basico1 {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		String[] nome = new String[n];
		int[] idade = new int[n];
		double[] altura = new double[n]; 
		double somaAltura = 0.0;
		int count = 0;
		
		for(int i=0; i<n; i++) {
			nome[i] = sc.next();
			idade[i] = sc.nextInt();
			altura[i] = sc.nextDouble();
			somaAltura += altura[i];
			
			if(idade[i] < 16) {
				count++;   
			}
		}
		
		for(String s : nome) {
			System.out.print(s + " ");
		}
		System.out.println();
		
		double mediaAltura = somaAltura / n;		
		System.out.printf("Altura média: %.2f%n", mediaAltura);
		
		double percent = (count * 100) / n; 
		System.out.println("Pessoas com menos de 16 anos: " + percent + "%");

		sc.close();
	}

}
