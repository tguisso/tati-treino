package br.com.tatiguisso.tatitreino.curso.membrosestaticos.util;

public class CurrencyConverter {

	public static final double TAX = 6.0;

	public static double amountToBePaidInReais(double price, double bought){
		double dollars =  bought * price;
		return dollars += dollars * TAX /100;
	}

}
