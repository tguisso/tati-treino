package br.com.tatiguisso.tatitreino.curso.exercicio01.domains;

public class Rectangle {
	
	public double width; //largura
	public double height; //altura
	
	public double area() { //A = w . h

		return width * height;
	}
	
	public double perimeter() { //P = 2(w + h)
		
		return (2 * (width + height));
	}
	
	public double diagonal() {  //d = raiz quadrada( w² + h²)

		return Math.sqrt(Math.pow(width,2)+ Math.pow(height, 2)) ;
	}
	
	public String toString() {
		
		return "AREA = " + String.format("%.2f%n", area())+
				"PERIMETER = " + String.format("%.2f%n", perimeter())+
				"DIAGONAL = " +String.format("%.2f%n", diagonal());
	}

}
