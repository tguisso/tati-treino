package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Locale;
import java.util.Scanner;

public class Vetor8 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		double[] altura = new double[n];
		char[] sexo = new char[n];
		double menor = 0;
		double maior = 0;
		double somaAlturaMulheres = 0.0;
		double mediaAlturaMulheres = 0.0;
		int qtdHomens = 0;
		int qtdMulheres = 0;
		
		for(int i=0; i<n; i++) {
			altura[i] = sc.nextDouble();
			sexo[i] = sc.next().charAt(0);
			menor = altura[0];
		}
		
		for(int i=0; i<n; i++) {
			if(altura[i] > maior) {
				maior = altura[i];
			}
			if(altura[i] < menor) {
				menor = altura[i];
			}
			if(sexo[i] == 'f') {
				somaAlturaMulheres += altura[i];
				qtdMulheres++;
			}
			
			if(sexo[i] == 'm') {
				qtdHomens++;
			}
			
						
		}
		
		mediaAlturaMulheres = somaAlturaMulheres / qtdMulheres;
		
		System.out.println("Menor altura: " + menor);
		System.out.println("Maior altura: " + maior);
		System.out.printf("Media das alturas das mulheres: %.2f%n", mediaAlturaMulheres);
		System.out.println("Numero de homens: "+ qtdHomens);
		
		sc.close();

	}

}
