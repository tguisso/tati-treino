package br.com.tatiguisso.tatitreino.curso.exercicio04.app;

import java.util.Locale;
import java.util.Scanner;

import br.com.tatiguisso.tatitreino.curso.exercicio04.entities.Account;

public class Program04 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		Account account;
		
		System.out.print("Enter account number: ");
		int number = sc.nextInt();
		System.out.print("Enter account holder: ");
		sc.nextLine();
		String holder = sc.nextLine(); 
		
		
		System.out.print("Is there an initial deposit (y/n)? ");
		String response = sc.nextLine();
		
		if (response.equals("y")) {
			System.out.print("Enter initial deposit value: ");
			double initialDeposit = sc.nextDouble();
			account = new Account(number, holder, initialDeposit);
		}
		else {
			account = new Account(number, holder);
		}

		System.out.println();
		System.out.println("Account data: ");
		System.out.println(account);
		
		System.out.println();
		System.out.print("Enter a deposit value: ");
		double depositValue = sc.nextDouble();
		account.deposit(depositValue);		
		System.out.println("Updated account data: ");
		System.out.println(account);
		
		System.out.println();
		System.out.print("Enter a withdraw value: ");
		double withdrawValue = sc.nextDouble();
		account.withdraw(withdrawValue);		
		System.out.println("Updated account data: ");
		System.out.println(account);
		
		
		sc.close();	

	}

}
