package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Locale;
import java.util.Scanner;

public class Vetor7 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		String[] aluno = new String[n];
		double[] nota1 = new double[n];
		double[] nota2 = new double[n];
		double media = 0.0;
		
		for(int i=0; i<n; i++) {
			aluno[i] = sc.next();
			nota1[i] = sc.nextDouble();
			nota2[i] = sc.nextDouble();
			
		}
		
		for(int i=0; i<n; i++) {
			media = (nota1[i] + nota2[i]) / 2;
			
			if(media >= 6) {
				System.out.println("Alunos aprovados: " + aluno[i]);
			}
		}
		
		
		
		sc.close();

	}

}
