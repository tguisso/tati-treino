package br.com.tatiguisso.tatitreino.curso.vetores.exercicio.app;

import java.util.Scanner;

import br.com.tatiguisso.tatitreino.curso.vetores.exercicio.entities.Quartos;

public class AppPensionato {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		 
		System.out.print("How many rooms will be rented? ");
		int n = sc.nextInt();
		System.out.println();

		Quartos[] vect = new Quartos[10];
		
		for(int i=0; i<n; i++) {
			sc.nextLine();
			int count = 1;
			count += i;
			System.out.println("Rent #" + count+ ":");
			
			System.out.print("Name: ");
			String name = sc.nextLine();
			
			System.out.print("Email: ");
			String email = sc.nextLine();
			
			System.out.print("Room: ");
			int room = sc.nextInt();
			vect[room] = new Quartos(name, email, room); 

			System.out.println();

		}

		sc.close();
	}

}
