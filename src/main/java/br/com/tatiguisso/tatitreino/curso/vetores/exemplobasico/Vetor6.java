package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Scanner;

public class Vetor6 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		String[] nome = new String[n];
		int[] idade = new int[n];
		int maiorIdade = idade[0];
		String maisVelho = null;
		
		for(int i=0; i<n; i++) {
			nome[i] = sc.next();
			idade[i] = sc.nextInt();
		}
		
		for(int i=0; i<n; i++) {
			if(idade[i] > maiorIdade) {
				maiorIdade = idade[i];
				maisVelho = nome[i];
			}
		}

		System.out.println("Pessoa mais Velha: " + maisVelho);
		
		sc.close();

	}

}
