package br.com.tatiguisso.tatitreino.curso.matrizes.exerciciobasico;

import java.util.Scanner;

public class Matrizes3 {

	public static void main(String[] args) {
		

		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int[][] mat = new int[n][n];
		int[] vet = new int[n];
		int maiorNum = 0;
		
		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		
		for(int i=0; i<n; i++) {
			maiorNum = 0;
			for(int j=0; j<n; j++) {
				if(mat[i][j] > maiorNum) {
					maiorNum = mat[i][j];
				}
			}
			System.out.println(maiorNum);
		}
		
		
		sc.close();

	}

}
