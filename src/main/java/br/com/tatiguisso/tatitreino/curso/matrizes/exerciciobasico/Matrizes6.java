package br.com.tatiguisso.tatitreino.curso.matrizes.exerciciobasico;

import java.util.Locale;
import java.util.Scanner;

public class Matrizes6 {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		double[][] mat = new double[n][n];
		double[][] matAlterada = new double[n][n];
		double soma = 0.0;

		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++) {
				mat[i][j] = sc.nextInt();
			}
		}

		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++) {
				if(mat[i][j] > 0) {
					soma += mat[i][j];
				}
			}
		}

		int linha = sc.nextInt();
		int coluna = sc.nextInt();
		double[] vetLinha = new double[n];
		double[] vetColuna = new double[n];


		System.out.println("SOMA DOS POSITIVOS: "+ soma);

		System.out.print("LINHA ESCOLHIDA: ");

		for(int i=linha; i==linha; i++) {
			for(int j=0; j<n; j++) {
				vetLinha[j] = mat[i][j];
				System.out.print(vetLinha[j] + " ");
			}
		}
		System.out.println();
		System.out.print("COLUNA ESCOLHIDA: ");

		for(int i=0; i<n; i++) {
			for(int j=coluna; j==coluna; j++) {
				vetColuna[i] = mat[i][j];
				System.out.print(vetColuna[i] + " ");
			}
		}

		System.out.println();
		System.out.print("DIAGONAL PRINCIPAL: ");

		for(int i=0; i<n; i++) {
			System.out.print(mat[i][i] + " ");
		}
		
		System.out.println();
		System.out.println("MATRIZ ALTERADA: ");
		
		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++) {
				if(mat[i][j] < 0) {
					mat[i][j]= Math.pow(mat[i][j], 2);
				}
				matAlterada[i][j] = mat[i][j];
				System.out.print(matAlterada[i][j]+ " ");
			}
			System.out.println();
		}
		
		sc.close();

	}

}
