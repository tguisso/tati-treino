package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Locale;
import java.util.Scanner;

public class Vetor9 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		String[] nomeMercadoria = new String[n];
		double[] precoCompra = new double[n];
		double[] precoVenda = new double[n];
		double[] lucro = new double[n];
		int lucroAbaixo_10 = 0;
		int lucroEntre_10_20 =0;
		int lucroAcima_20 =0;
		double valorTotalCompra = 0.0;
		double valorTotalVenda = 0.0;
		double lucroTotal = 0.0;
		
		for(int i=0; i<n; i++) {
			nomeMercadoria[i] = sc.next();
			precoCompra[i] = sc.nextDouble();
			precoVenda[i] = sc.nextDouble();
			valorTotalCompra += precoCompra[i];
			valorTotalVenda += precoVenda[i];
		}
		
		for(int i=0; i<n; i++) {
			lucro[i] = ((precoVenda[i] - precoCompra[i])/precoCompra[i])*100;
			lucroTotal += (precoVenda[i] - precoCompra[i]);
			
			if(lucro[i] < 10) {
				lucroAbaixo_10++;
			}
			else if(lucro[i] >= 10 && lucro[i] <= 20) {
				lucroEntre_10_20++;
			}
			else {
				lucroAcima_20++;
			}				
		}
		
		System.out.println("Lucro abaixo de 10%: "+lucroAbaixo_10);
		System.out.println("Lucro entre 10% e 20%: "+lucroEntre_10_20);
		System.out.println("Lucro acima de 20%: "+lucroAcima_20);
		System.out.printf("Valor total de compra: %.2f%n",valorTotalCompra);
		System.out.printf("Valor total de venda: %.2f%n",valorTotalVenda);
		System.out.printf("Lucro total: %.2f%n", lucroTotal);
		
		sc.close();

	}

}
