package br.com.tatiguisso.tatitreino.curso.exercicio01.app;

import java.util.Locale;
import java.util.Scanner;

import br.com.tatiguisso.tatitreino.curso.exercicio01.domains.Rectangle;

public class Main {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		Rectangle rectangle = new Rectangle();
		
		System.out.println("Enter rectangle width and height: ");
		rectangle.width = sc.nextDouble();
		rectangle.height = sc.nextDouble();
		
		System.out.println(rectangle.toString());
		
		
		sc.close();

	}

}
