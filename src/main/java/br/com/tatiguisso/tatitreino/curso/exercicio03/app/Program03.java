package br.com.tatiguisso.tatitreino.curso.exercicio03.app;

import java.util.Locale;
import java.util.Scanner;

import br.com.tatiguisso.tatitreino.curso.exercicio03.domains.Student;

public class Program03 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		Student student = new Student();
		
		System.out.println("Informe nome do Aluno");
		student.name = sc.nextLine();
		
		System.out.println("Informe as notas do Aluno: ");
		student.grade1 = sc.nextDouble();
		student.grade2 = sc.nextDouble();
		student.grade3 = sc.nextDouble();
		
		System.out.printf("FINAL GRADE: %.2f%n", student.finalGrade());
		
		if (student.finalGrade() < 60) {
			System.out.println("FAILED");
			System.out.printf("MISSING %.2f POINTS%n", student.missingGrade());
		}
		else {
			System.out.println("PASS");
		}
		
			
		sc.close();

	}

}
