package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Scanner;

public class Vetor1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[] vet = new int[n];

		int maior = vet[0];
		int posicaoMaior = 0;

		for(int i=0; i<n; i++) {
			vet[i] = sc.nextInt();

			if(vet[i] > maior) {
				maior = vet[i];
				posicaoMaior = i;
			}
		}

		System.out.println("Maior numero " + maior);
		System.out.println("posição do maior "+ posicaoMaior);

		sc.close();

	}

}
