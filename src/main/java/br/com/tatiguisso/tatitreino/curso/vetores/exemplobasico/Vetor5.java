package br.com.tatiguisso.tatitreino.curso.vetores.exemplobasico;

import java.util.Scanner;

public class Vetor5 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int[] vet = new int[n];
		int count = 0;
		int pares = 0;
		int media = 0;
		
		for(int i=0; i<n; i++) {
			vet[i] = sc.nextInt();
		}
		
		for(int i=0; i<n; i++) {
			if(vet[i] % 2 == 0) {
				pares += vet[i];
				count++;
			}
		}
		media = pares / count;
		System.out.println(media);
		
		
		sc.close();

	}

}
