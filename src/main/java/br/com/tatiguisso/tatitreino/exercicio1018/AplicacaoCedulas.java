package br.com.tatiguisso.tatitreino.exercicio1018;

import java.util.Scanner;

public class AplicacaoCedulas {

	public static void main(String[] args) {
		Cedulas calcularQtdCedulas = new Cedulas();
		Scanner sc = new Scanner(System.in);
		
		int valorCedula;
		int qtdCedula;
		int cedula;
		int resto;
		
		valorCedula = sc.nextInt();
		
		
		cedula = 100;
		qtdCedula = valorCedula / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = valorCedula % cedula;
		
		cedula = 50;
		qtdCedula = resto / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = resto % cedula;
		
		cedula = 20;
		qtdCedula = resto / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = resto % cedula;
		
		cedula = 10;
		qtdCedula = resto / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = resto % cedula;
		
		cedula = 5;
		qtdCedula = resto / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = resto % cedula;
		
		cedula = 2;
		qtdCedula = resto / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = resto % cedula;
		
		cedula = 1;
		qtdCedula = resto / cedula;
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		resto = resto % cedula;
		
		sc.close();

	}
		
}
