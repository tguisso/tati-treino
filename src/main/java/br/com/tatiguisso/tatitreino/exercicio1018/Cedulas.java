package br.com.tatiguisso.tatitreino.exercicio1018;

public class Cedulas {

	public int calcularQtdCedulas (int valorCedula, int qtdCedula, int cedula, int resto) {
		
		cedula = 100;//FIXME: remover
		resto = process(cedula, valorCedula);
		
		cedula = 50;
		resto = process(cedula, resto);
		
		cedula = 20;
		resto = process(cedula, resto);
		
		//TODO: Continuar daqui!!! SFDA!
		cedula = 10;
		resto = process(cedula, resto);
		
		cedula = 5;
		resto = process(cedula, resto);
		
		cedula = 2;
		resto = process(cedula, resto);
		
		cedula = 1;
		resto = process(cedula, resto);
		
		return qtdCedula;

	}
	
	private int calcularResto (int restoAnterior, int cedula) {
		int resultado = restoAnterior % cedula;
		return resultado;
	}
	
	private int calcularQtdCedula (int restoAnterior, int cedula) {
		int resultado = restoAnterior / cedula;
		return resultado;
	}

	//FIXME: Definir um melhor nome para este metodo
	private int process(int cedula, int valorCedula) {

		int qtdCedula = calcularQtdCedula(valorCedula, cedula);
		System.out.println(qtdCedula + " nota(s) de R$ " + cedula );
		int resto = calcularResto(valorCedula, cedula);
		
		return resto;
	}
	
}
