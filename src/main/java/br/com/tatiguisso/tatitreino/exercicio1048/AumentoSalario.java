package br.com.tatiguisso.tatitreino.exercicio1048;

import java.util.Locale;
import java.util.Scanner;

public class AumentoSalario {


	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double salario = sc.nextDouble();
		double novoSalario;
		double reajuste;
		String percentual;
		
		if (salario >= 0 && salario <= 400.00) {
			percentual = "15 %";
			reajuste = salario * 0.15;
			novoSalario = reajuste + salario;
			System.out.printf("Novo Salario: %.2f%nReajuste ganho: %.2f%nEm percentual: %s%n", 
					novoSalario, reajuste, percentual);
		}
		else if (salario >= 400.01 && salario <= 800.00) {
			percentual = "12 %";
			reajuste = salario * 0.12;
			novoSalario = reajuste + salario;
			System.out.printf("Novo Salario: %.2f%nReajuste ganho: %.2f%nEm percentual: %s%n", 
					novoSalario, reajuste, percentual);
		}
		else if (salario >= 800.01 && salario <= 1200.00) {
			percentual = "10 %";
			reajuste = salario * 0.10;
			novoSalario = reajuste + salario;
			System.out.printf("Novo Salario: %.2f%nReajuste ganho: %.2f%nEm percentual: %s%n", 
					novoSalario, reajuste, percentual);
		}
		else if (salario >= 1200.01 && salario <= 2000.00) {
			percentual = "7 %";
			reajuste = salario * 0.07;
			novoSalario = reajuste + salario;
			System.out.printf("Novo Salario: %.2f%nReajuste ganho: %.2f%nEm percentual: %s%n", 
					novoSalario, reajuste, percentual);
		}
		else {
			percentual = "4 %";
			reajuste = salario * 0.04;
			novoSalario = reajuste + salario;
			System.out.printf("Novo Salario: %.2f%nReajuste ganho: %.2f%nEm percentual: %s%n", 
					novoSalario, reajuste, percentual);
		}
		
		sc.close();

	}

}
