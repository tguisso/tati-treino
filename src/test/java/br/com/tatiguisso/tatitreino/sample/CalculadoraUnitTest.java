package br.com.tatiguisso.tatitreino.sample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculadoraUnitTest {

	@Test
	public void somaComSucesso() {
		final int resultadoEsperado = 25;
		
		Calculadora minhaCalc = new Calculadora();
		
		int a = 10;
		int b = 15;
		
		int resultado = minhaCalc.soma(a, b);
		
		assertEquals(resultadoEsperado, resultado);
		
	}
	
	@Test
	public void somaComSucessoOtherCase() {
		final int resultadoEsperado = 50;
		
		Calculadora minhaCalc = new Calculadora();
		
		int a = 40;
		int b = 10;
		
		int resultado = minhaCalc.soma(a, b);
		
		assertEquals(resultadoEsperado, resultado);
		
	}
	
}
