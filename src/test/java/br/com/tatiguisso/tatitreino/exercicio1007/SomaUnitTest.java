package br.com.tatiguisso.tatitreino.exercicio1007;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SomaUnitTest {
	
	Soma soma = new Soma();
	
	@Test
	public void case01() {
		
		int resultado = soma.somar(6, 2, 3, 4);
		assertEquals(15, resultado, 0);
	}
	
	@Test
	public void case02() {
		
		int resultado = soma.somar(1, 49, 83, 6);
		assertEquals(139, resultado, 0);
	}

}
