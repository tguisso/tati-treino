package br.com.tatiguisso.tatitreino.exercicio1007;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import junit.framework.Assert;

public class SubtracaoUnitTest {

	Subtracao subtracao = new Subtracao();
	
	@Test
	public void case01() {
		
		int resultado = subtracao.subtrair(8, 2, 5, 4);
		assertEquals(-3, resultado, 0);
		
	}
	
	@Test
	public void case02() {
		
		int resultado = subtracao.subtrair(27, 4, 9, 2);
		assertEquals(12, resultado, 0);
	
	}

}
