package br.com.tatiguisso.tatitreino.exercicio1007;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DiferencaUnitTest {
	
	@Test
	public void test1() {
		Diferenca diferenca = new Diferenca();
		int param1 = 5;
		int param2 = 3;
		int param3 = 7;
		int param4 = 9;
		
		int resultadoEsperado = -48;
		
		int resultadoCalculado = diferenca.execute(param1, param2, param3, param4);
		
		assertEquals(resultadoEsperado, resultadoCalculado);
	}
	
	@Test
	public void test2() {
		Diferenca diferenca = new Diferenca();
		int param1 = 0;
		int param2 = 4;
		int param3 = 7;
		int param4 = 2;
		
		int resultadoEsperado = -14;
		
		int resultadoCalculado = diferenca.execute(param1, param2, param3, param4);
		
		assertEquals(resultadoEsperado, resultadoCalculado);
	}
	
	@Test
	public void test3() {
		Diferenca diferenca = new Diferenca();
		int param1 = 10;
		int param2 = 15;
		int param3 = 2;
		int param4 = 2;
		
		int resultadoEsperado = 146;
		
		int resultadoCalculado = diferenca.execute(param1, param2, param3, param4);
		
		assertEquals(resultadoEsperado, resultadoCalculado);
	}
	
	@Test
	public void test4() {
		Diferenca diferenca = new Diferenca();
		int param1 = 0;
		int param2 = 0;
		int param3 = 7;
		int param4 = 8;
		
		int resultadoEsperado = -56;
		
		int resultadoCalculado = diferenca.execute(param1, param2, param3, param4);
		
		assertEquals(resultadoEsperado, resultadoCalculado);
	}
	

}
