package br.com.tatiguisso.tatitreino.exercicio1007;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

public class OrquestradorUnitTest {

	@InjectMocks
	Orquestrador orquestrador = new Orquestrador();

	@Mock
	private Soma somador;

	@Mock
	private Subtracao subtracao;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void somaMaior10() {

//		doReturn(11).when(this.somador).somar(0, 0, 0, 0);
//		String resultado = orquestrador.executar(0, 0, 0, 0); 
//		assertEquals("sucesso",resultado);
		
		int num1 = 0;
		int num2 = 2;
		int num3 = 3;
		int num4 = 5;
		
		verify(this.somador).somar(num1, num2, num3, num4);
	}

	@Test
	public void somaMenor10() {

		doReturn(9).when(this.somador).somar(0, 0, 0, 0);
		doReturn(2).when(this.subtracao).subtrair(0, 0, 0, 0);

		String resultado = orquestrador.executar(0, 0, 0, 0);
		assertEquals("invalido", resultado);
	}

	@Test
	public void diferencaMenor1() {

		doReturn(9).when(this.somador).somar(0, 0, 0, 0);
		doReturn(-1).when(this.subtracao).subtrair(0, 0, 0, 0);

		String resultado = orquestrador.executar(0, 0, 0, 0);
		assertEquals("erro", resultado);
	}

}
